import 'package:flutter/material.dart';
import 'package:restsuitapp/utility/app_style.dart';

class BoxStyle{
  static var headerBox = BoxDecoration(
    color: AppStyle().yellowDarkColor,
    borderRadius: BorderRadius.circular(0),
    boxShadow: [
      BoxShadow(
        color: AppStyle().yellowDarkColor,
        blurRadius: 5,
        offset: const Offset(0, 5), // changes position of shadow
      ),
    ],
  );
  static var whiteBoxBorderBlue10 = BoxDecoration(
      color: AppStyle().greenColor,
      borderRadius: const BorderRadius.all(
        Radius.circular(10),
      ),
      border: Border.all(color: AppStyle().greyColor));

}